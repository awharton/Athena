/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_RpcMeasurementFWD_H
#define XAODMUONPREPDATA_RpcMeasurementFWD_H


/** @brief Forward declaration of the xAOD::RpcMeasurement */
namespace xAOD{
   class RpcMeasurement_v1;
   using RpcMeasurement = RpcMeasurement_v1;
}
#endif
