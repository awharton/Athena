#include "../EventDuplicateFinderAlg.h"
#include "../EventInfoCnvAlg.h"
#include "../EventInfoCnvTool.h"
#include "../EventInfoOverlay.h"
#include "../EventInfoUpdateFromContextAlg.h"
#include "../EventInfoReaderAlg.h"

DECLARE_COMPONENT( xAODMaker::EventInfoCnvAlg )
DECLARE_COMPONENT( xAODMaker::EventInfoCnvTool )
DECLARE_COMPONENT( xAODMaker::EventInfoOverlay )
DECLARE_COMPONENT( xAODMaker::EventInfoUpdateFromContextAlg )
DECLARE_COMPONENT( xAODReader::EventDuplicateFinderAlg )
DECLARE_COMPONENT( xAODReader::EventInfoReaderAlg )
