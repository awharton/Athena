/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#ifndef JIVEXML_XAODTAURETRIEVER_H
#define JIVEXML_XAODTAURETRIEVER_H

#include <string>
#include <vector>
#include <map>

#include "JiveXML/IDataRetriever.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "xAODTau/TauJetContainer.h"

namespace JiveXML{

  /**
   * @class xAODTauRetriever
   * @brief Retrieves all @c Tau @c objects (TauAODCollection etc.)
   *
   *  - @b Properties
   *    - PriorityTauCollection: First collection to be retrieved, displayed
   *      in Atlantis without switching. All other collections are
   *      also retrieved.
   *    - OtherTauCollections
   *    - DoWriteHLT
   *    - DoWriteAllCollections
   *    - TracksName
   *
   *  - @b Retrieved @b Data
   *    - Usual four-vectors: phi, eta, et etc.
   *    - Associations for clusters and tracks via ElementLink: key/index scheme
   */
  class xAODTauRetriever : virtual public IDataRetriever,
			   public AthAlgTool {

  public:

    /// Standard Constructor
    xAODTauRetriever(const std::string& type,const std::string& name,const IInterface* parent);

    virtual StatusCode retrieve(ToolHandle<IFormatTool> &FormatTool);
    /// Return the name of the data type that is generated by this retriever
    const DataMap getData(const xAOD::TauJetContainer*);
    /// Gets the StoreGate keys for the desired containers
    const std::vector<std::string> getKeys();
    /// Return the name of the data type that is generated by this retriever
    virtual std::string dataTypeName() const { return m_typeName; };

  private:

    /// The data type that is generated by this retriever
    const std::string m_typeName = "TauJet";

    Gaudi::Property<bool> m_doWriteHLT {this, "DoWriteHLT", false, "Write out other collections that have HLT in the name"};
    Gaudi::Property<bool> m_doWriteAllCollections {this, "DoWriteAllCollections", false, "Write out all Cluster collections"};
    Gaudi::Property<std::vector<std::string>> m_otherKeys {this, "OtherTauCollections", {}, "Other collections to be retrieved. If DoWriteAllCollections is set to true all available Tau collections will be retrieved"};
    Gaudi::Property<std::string> m_priorityKey {this,"PriorityTauCollection","TauJets", "Name of the priority Tau container that will be written out first"};
    Gaudi::Property<std::string>  m_tracksName {this, "TracksName", "InDetTrackParticles_xAOD", "Name of track container"};

  };
}
#endif
