/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "../FullMenu.h"
#include "../RatesEmulationExample.h"
#include "../L1TopoRatesCalculator.h"

DECLARE_COMPONENT( FullMenu )
DECLARE_COMPONENT( RatesEmulationExample )
DECLARE_COMPONENT( L1TopoRatesCalculator )
